package grafos;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.comments.Comment;
import com.github.javaparser.ast.visitor.VoidVisitor;

import java.io.File;
import java.io.FileNotFoundException;

public class Transformador {

	public static void main(String[] args) throws FileNotFoundException {
		File file = new File(args[0]);
		if (file.isDirectory())
			analyzeDir(file);
		else
			analyzeFile(file);
	}

	/**
	 * Runs {@link #analyzeFile(File)} for each Java file found in this directory, recursively.
	 */
	public static void analyzeDir(File dir) throws FileNotFoundException {
		for (File f : dir.listFiles()) {
			if (f.isDirectory())
				analyzeDir(f);
			else if (f.getName().endsWith(".java"))
				analyzeFile(f);
		}
	}

	/** Analyzes and generates a CFG in a PDF from a Java file. */
	public static void analyzeFile(File file) throws FileNotFoundException {
		System.out.println("BEGIN FILE " + file.getPath());
		// Ruta del fichero con el programa que vamos a transformar
		String ruta = file.getPath().substring(0, file.getPath().lastIndexOf(".java"));

		// Abrimos el fichero original (un ".java")
		File original = new File(ruta + ".java");

		// Parseamos el fichero original. Se crea una unidad de compilación (un AST).
		CompilationUnit cu = JavaParser.parse(original);

		quitarComentarios(cu);

		// Recorremos el AST
		CFG graph = new CFG();
		VoidVisitor<CFG> visitador = new Visitador();
		visitador.visit(cu, graph);

		// Imprimimos el CFG del program
		GraphViz gv = new GraphViz();
		String dotInfo = String.join("\n", graph.toStringList(gv));

		// Generamos un PDF con el CFG del programa
		System.out.print("\nGenerando PDF...");
		gv.add(dotInfo);
		String type = "pdf";   // String type = "gif";
		// gv.increaseDpi();
		gv.decreaseDpi();
		gv.decreaseDpi();
		gv.decreaseDpi();
		gv.decreaseDpi();
		File destino_CFG = new File(ruta + "_CFG." + type);
		gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), type), destino_CFG);
		System.out.println("     PDF generado!");
	}

	// Elimina todos los comentarios de un nodo y sus hijos
	private static void quitarComentarios(Node node) {
		node.removeComment();
		for (Comment comment : node.getOrphanComments()) {
			node.removeOrphanComment(comment);
		}
		// Do something with the node
		for (Node child : node.getChildNodes()) {
			quitarComentarios(child);
		}
	}

}
